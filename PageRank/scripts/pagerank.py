#! /usr/bin/env python
#
# -*- coding: utf-8 -*-

"""Runnable script for the PageRank project."""

__author__ = "rlazo.paz@gmail.com (Rodrigo Lazo)"

import subprocess
import sys
import itertools
import os
from xml.etree import ElementTree


# Configuration
HADOOP_BIN="hadoop"
JAR = "PageRank.jar"
CLASS = "org.rlazo.PageRank"
LIB_JARS = ()
ARGS = ()


# Code - do not modify
def parseConfigurationFile(filename):
    """Extracts the name-value pair from a hadoop configuration file.

    Arguments:
    - `filename`: Path to the configuration file.

    Returns:
      A list containing the name value pairs separated by whitespace.
    """
    root = ElementTree.parse(filename).getroot()
    entries = []
    for item in root.getiterator("property"):
        entries.append("%s %s" % (item.find("name").text,
                                  item.find("value").text))
    return entries


def buildCommand(args):
    """Constructs the command to run.

    Args:
    - `args`: list of string of extra arguments to use.

    Returns:
      String storing the command to execute.
    """
    command = "%s jar %s %s " % (HADOOP_BIN, JAR, CLASS)
    if LIB_JARS:
        command += " -libjars %s" % ",".join(LIB_JARS)
    return command + " ".join(itertools.chain(ARGS, args))


if __name__ == "__main__":
    if len(sys.argv) != 2:
        print "Required configuration file name"
        sys.exit(-1)
    command = buildCommand(parseConfigurationFile(sys.argv[1]))
    print "[COMMAND] " + command
    subprocess.call(command, shell=True)
