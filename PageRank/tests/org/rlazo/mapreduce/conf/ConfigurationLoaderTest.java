package org.rlazo.mapreduce.conf;

import org.apache.hadoop.conf.Configuration;
import org.junit.Assert;
import org.junit.Test;
import org.rlazo.mapreduce.conf.ConfigurationLoader;


public class ConfigurationLoaderTest {    
    @Test
    public void testLoadConfFromArgs() {
        String[] args = {"this", "is", "a", "pagerank.counter", "5", "test",
                "pagerank.mapper", "example", "pagerank.status", "true"};
        ConfigurationLoader loader = new ConfigurationLoader("pagerank");
        Configuration conf = loader.loadFromArgs(args);
        Assert.assertEquals(conf.get("pagerank.counter"), "5");
        Assert.assertEquals(conf.get("pagerank.mapper"), "example");
        Assert.assertEquals(conf.get("pagerank.status"), "true");
    }
}