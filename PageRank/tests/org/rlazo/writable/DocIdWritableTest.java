package org.rlazo.writable;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import org.rlazo.TestHelpers;
import org.rlazo.writable.DocIdWritable;

public class DocIdWritableTest {
    private static String TEST_URI = "http://www.google.com";
    private static String LESSER_URI = "http://www.amazon.com";
    private static String GREATER_URI = "http://www.unicamp.br";
    private DocIdWritable docId = new DocIdWritable();

    @Before
    public void setUp() {
        docId.set(TEST_URI);
    }

    @Test
    public void constructorWithArguments() {
        DocIdWritable obj = new DocIdWritable(TEST_URI);
        Assert.assertEquals(TEST_URI, obj.getURI());
    }

    @Test
    public void defaultConstructor() {
        DocIdWritable obj = new DocIdWritable();
        Assert.assertEquals(obj.getURI(), "");
    }

    @Test
    public void serializationAndDeserialization() throws IOException {
        DocIdWritable original = new DocIdWritable(TEST_URI);
        DocIdWritable derived = new DocIdWritable();
        TestHelpers.checkSerialization(original, derived);
        Assert.assertEquals("URIs are not equals", original.getURI(),
                            derived.getURI());
    }

    @Test
    public void emptyserializationAndDeserialization() throws IOException {
        DocIdWritable original = new DocIdWritable();
        DocIdWritable derived = new DocIdWritable();
        TestHelpers.checkSerialization(original, derived);
        Assert.assertEquals("URIs are not equals",
                original.getURI(), derived.getURI());
    }
    
    @Test
    public void factoryTest() throws IOException {
        DocIdWritable original = new DocIdWritable(TEST_URI);
        ByteArrayOutputStream buffer = new ByteArrayOutputStream();
        original.write(TestHelpers.wrapAsDataOutput(buffer));
        Assert.assertEquals(original, 
                DocIdWritable.read(TestHelpers.wrapAsDataInput(buffer))); 
    }

    @Test
    public void comparation() {
        DocIdWritable lesserDocId = new DocIdWritable(LESSER_URI);
        DocIdWritable greaterDocId = new DocIdWritable(GREATER_URI);
        Assert.assertTrue(docId.compareTo(lesserDocId) > 0);
        Assert.assertTrue(lesserDocId.compareTo(docId) < 0);
        Assert.assertTrue(docId.compareTo(greaterDocId) < 0);
        Assert.assertTrue(greaterDocId.compareTo(docId) > 0);
        Assert.assertTrue(docId.compareTo(docId) == 0);
    }

    @Test
    public void equality() {
        DocIdWritable lesserDocId = new DocIdWritable(LESSER_URI);
        Assert.assertFalse(docId.equals(lesserDocId));
        Assert.assertFalse(docId.equals(23));
        Assert.assertFalse(docId.equals(null));
        Assert.assertTrue(docId.equals(docId));        
    }
}
