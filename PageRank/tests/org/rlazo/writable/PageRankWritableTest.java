package org.rlazo.writable;

import java.io.IOException;

import org.junit.Assert;
import org.junit.Test;
import org.rlazo.TestHelpers;
import org.rlazo.writable.DocIdWritable;
import org.rlazo.writable.PageRankWritable;

public class PageRankWritableTest {
    private static final DocIdWritable[] DOC_ID_ARRAY = new DocIdWritable[3];
    private static final double TEST_PR = 0.32d;
    private static final double EPSILON = 0.00001d;

    static {
        DOC_ID_ARRAY[0] = new DocIdWritable("http://www.amazon.com");
        DOC_ID_ARRAY[1] = new DocIdWritable("http://www.google.com");
        DOC_ID_ARRAY[2] = new DocIdWritable("http://www.unicamp.br");
    }

    @Test
    public void constructorWithArguments() {
        PageRankWritable obj = new PageRankWritable();
        Assert.assertEquals(obj.getPageRank(), PageRankWritable.DEFAULT_PR, EPSILON);
    }

    @Test
    public void defaultConstructor() {
        PageRankWritable obj = new PageRankWritable(TEST_PR, DOC_ID_ARRAY);
        Assert.assertEquals(obj.getPageRank(), TEST_PR, EPSILON);
        Assert.assertArrayEquals(obj.getOutgoingLinks(), DOC_ID_ARRAY);
    }

    @Test
    public void serializationAndDeserialization() throws IOException {
        PageRankWritable original = new PageRankWritable(TEST_PR, DOC_ID_ARRAY);
        PageRankWritable derived = new PageRankWritable();
        TestHelpers.checkSerialization(original, derived);
        Assert.assertEquals(original.getPageRank(), derived.getPageRank(), EPSILON);
        Assert.assertArrayEquals(original.getOutgoingLinks(),
                                 derived.getOutgoingLinks());
    }

    @Test
    public void emptyserializationAndDeserialization() throws IOException {
        PageRankWritable original = new PageRankWritable();
        PageRankWritable derived = new PageRankWritable();
        TestHelpers.checkSerialization(original, derived);
        Assert.assertEquals(original.getPageRank(), derived.getPageRank(), EPSILON);
        Assert.assertArrayEquals(original.getOutgoingLinks(),
                                 derived.getOutgoingLinks());
    }
}
