package org.rlazo.writable;

import java.io.IOException;

import org.junit.Assert;
import org.junit.Test;
import org.rlazo.TestHelpers;
import org.rlazo.writable.PageRankIntermediateWritable;
import org.rlazo.writable.PageRankWritable;

public class PageRankIntermediateWritableTest {
    private static final double EPSILON = 0.000001d;
    private static final double VALID_PR_C = 0.1235d;
    private static final double INVALID_PR_C = -1.0d;  // PageRank coannot be negative

    @Test
    public void defaultConstructor() {
        PageRankIntermediateWritable obj = new PageRankIntermediateWritable();
        Assert.assertEquals(obj.getDefaultPageRankRatio(),
                            obj.getPageRankCounterRatio(), EPSILON);
        Assert.assertEquals(obj.getPageRankWritable(), null);
    }

    @Test
    public void constructorWithValidArguments() throws InvalidPageRankRatioException {
        PageRankIntermediateWritable obj = new PageRankIntermediateWritable(VALID_PR_C);
        Assert.assertEquals(obj.getPageRankCounterRatio(), VALID_PR_C, EPSILON);
        PageRankWritable reference_obj = new PageRankWritable();
        obj = new PageRankIntermediateWritable(reference_obj);
        Assert.assertEquals(obj.getPageRankWritable(), reference_obj);
    }

    @Test (expected=InvalidPageRankRatioException.class)
    public void constructorWithInvalidArguments() throws InvalidPageRankRatioException {
        new PageRankIntermediateWritable(INVALID_PR_C);
    }

    @Test (expected=InvalidPageRankRatioException.class)
    public void setWithInvalidArguments() throws InvalidPageRankRatioException {
        PageRankIntermediateWritable obj = new PageRankIntermediateWritable();
        obj.set(INVALID_PR_C, null);
    }

    @Test
    public void serializationAndDeserialization() throws IOException, InvalidPageRankRatioException {
        PageRankIntermediateWritable original = new PageRankIntermediateWritable();
        PageRankWritable reference_obj = new PageRankWritable();
        original.set(VALID_PR_C, reference_obj);
        PageRankIntermediateWritable derived = new PageRankIntermediateWritable();
        TestHelpers.checkSerialization(original, derived);
    }

    @Test
    public void serializationAndDeserializationNoReference() throws IOException, InvalidPageRankRatioException {
        PageRankIntermediateWritable original = new PageRankIntermediateWritable();
        //original.set(VALID_PR_C, null);
        PageRankIntermediateWritable derived = new PageRankIntermediateWritable();
        TestHelpers.checkSerialization(original, derived);
    }
}
