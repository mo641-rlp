package org.rlazo.writable;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.rlazo.TestHelpers;
import org.rlazo.writable.DocIdArrayWritable;
import org.rlazo.writable.DocIdWritable;

public class DocIdArrayWritableTest {
    DocIdWritable[] docIdsArray;
    DocIdArrayWritable writableArray = new DocIdArrayWritable();

    @Before
    public void setUp() {
        docIdsArray = createDocIdArray("http://www.unicamp.br",
                                       "http://www.google.com",
                                       "http://www.amazon.com");
        writableArray.set(docIdsArray);
    }

    @Test
    public void defaultConstructor() {
        DocIdArrayWritable obj = new DocIdArrayWritable();
        obj.set(docIdsArray);
        Arrays.equals((Object [])docIdsArray, (Object [])obj.toArray());
    }

    @Test
    public void constructorWithArguments() {
        DocIdArrayWritable obj = new DocIdArrayWritable(docIdsArray);
        Arrays.equals((Object [])docIdsArray, (Object [])obj.toArray());
    }

    @Test
    public void serializationAndDeserialization() throws IOException {
        ByteArrayOutputStream buffer = new ByteArrayOutputStream();
        writableArray.write(TestHelpers.wrapAsDataOutput(buffer));
        DocIdArrayWritable newObj = DocIdArrayWritable.read(
            TestHelpers.wrapAsDataInput(buffer));
        Assert.assertArrayEquals((DocIdWritable [])writableArray.toArray(),
                (DocIdWritable [])newObj.toArray());
    }

    @Test
    public void emptySerializationAndDeserialization() throws IOException {
        DocIdArrayWritable emptyObj = new DocIdArrayWritable();
        ByteArrayOutputStream buffer = new ByteArrayOutputStream();
        emptyObj.write(TestHelpers.wrapAsDataOutput(buffer));
        DocIdArrayWritable newObj = DocIdArrayWritable.read(
            TestHelpers.wrapAsDataInput(buffer));
        Assert.assertArrayEquals((DocIdWritable [])emptyObj.toArray(),
                (DocIdWritable [])newObj.toArray());
    }

    public DocIdWritable[] createDocIdArray(String... uris) {
        List<DocIdWritable> list = new ArrayList<DocIdWritable>();
        for (String uri: uris) {
            list.add(new DocIdWritable(uri));
        }
        DocIdWritable[] array = new DocIdWritable[list.size()];
        return list.toArray(array);
    }
}
