package org.rlazo;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInput;
import java.io.DataInputStream;
import java.io.DataOutput;
import java.io.DataOutputStream;
import java.io.IOException;

import org.apache.hadoop.io.Writable;
import org.junit.Assert;

public class TestHelpers {
    public static DataOutput wrapAsDataOutput(ByteArrayOutputStream buffer) {
        return new DataOutputStream(buffer);
    }

    public static DataInput wrapAsDataInput(ByteArrayOutputStream buffer) {
        byte[] array = buffer.toByteArray();
        return new DataInputStream(new ByteArrayInputStream(array));
    }

    public static void checkSerialization(Writable original, Writable derived) throws IOException {
        ByteArrayOutputStream buffer = new ByteArrayOutputStream();
        original.write(TestHelpers.wrapAsDataOutput(buffer));
        derived.readFields(TestHelpers.wrapAsDataInput(buffer));
        Assert.assertEquals("Objects are not equal", original, derived);
    }
}
