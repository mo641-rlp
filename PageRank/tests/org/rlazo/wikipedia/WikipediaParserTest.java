package org.rlazo.wikipedia;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;

import javax.xml.stream.XMLStreamException;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.google.common.io.CharStreams;


public class WikipediaParserTest {
    private static final String TESTFILE_URL;
    private WikipediaParser parser;

    static {
        StringBuffer buffer = new StringBuffer();
        buffer.append("resources").append(File.separator);
        buffer.append("tests").append(File.separator);
        buffer.append("wiki_page.xml");
        TESTFILE_URL = buffer.toString();
    }

    @Before
    public void setUp() {
        parser = new WikipediaParser();
    }

    @Test
    public void normalizeXMLTest() {
        Assert.assertEquals(parser.normalizeXML("simple text"),
                            "<xml>simple text</xml>");
        Assert.assertEquals(parser.normalizeXML("<xml>already xml valid</xml>"),
                            "<xml>already xml valid</xml>");
        Assert.assertEquals(parser.normalizeXML("   <xml>   already valid</xml>  "),
                            "   <xml>   already valid</xml>  ");
        Assert.assertEquals(parser.normalizeXML(""),
                            "<xml></xml>");
    }

    @Test
    public void normalizeTitleTest() {
        Assert.assertEquals(parser.normalizeTitle("a long title"),
                            "a_long_title");
        Assert.assertEquals(parser.normalizeTitle("  a title(brackets)"),
                            "a_title(brackets)");
        Assert.assertEquals(parser.normalizeTitle(" User:Jimbo_ __ Wales  "),
                            ("User:Jimbo_Wales"));
    }

    @Test
    public void fullTest() throws IOException, XMLStreamException {
        String[] expected_links = {
            "July_17", "1932", "Terre_Haute,_Indiana", "basketball", "1954_NBA_Draft",
            "Los_Angeles_Lakers", "Minneapolis,_Minnesota", "Los_Angeles,_California",
            "player-coach", "Baltimore,_Maryland", "American_Basketball_Association",
            "Indiana_Pacers", "NBA", "WIBC_(FM)", "Category:Living_people",
            "Category:1932_births", "Category:American_basketball_coaches",
            "Category:American_basketball_players", "Category:Minneapolis_Lakers_players",
            "Category:Chicago_Packers_players", "Category:Chicago_Zephyrs_coaches",
            "Category:Baltimore_Bullets_coaches", "Category:Player-coaches"};
        String expected_title = "Bobby_Leonard";
        String tmp = CharStreams.toString(new InputStreamReader(
                new FileInputStream(TESTFILE_URL)));
        WikipediaParser parser = new WikipediaParser();
        WikipediaPage page = parser.parse(tmp);
        Assert.assertEquals(page.getTitle(), expected_title);
        Assert.assertArrayEquals(page.getLinks().toArray(), expected_links);
    }

}
