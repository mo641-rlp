package org.rlazo.writable;

public class InvalidPageRankRatioException extends Throwable {

    private static final long serialVersionUID = 6281972751762049120L;

    public InvalidPageRankRatioException() {
        super("PageRange Counter ratio set to an invalid value.");
    }
}
