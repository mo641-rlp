package org.rlazo.writable;

import java.io.DataInput;
import java.io.IOException;

import org.apache.hadoop.io.ArrayWritable;

import com.google.common.base.Joiner;

/**
 * Writable array of document's ids.
 *
 * Simple wrapper around ArrayWritable to make it support
 * DocIdWritable objects. If when created no array is provided, a
 * 0-length array is assumed for validity purposes.
 *
 * @author Rodrigo Lazo - RA109228
 */
public class DocIdArrayWritable extends ArrayWritable {
    public DocIdArrayWritable() {
        super(DocIdWritable.class, new DocIdWritable[0]);
    }

    public DocIdArrayWritable(DocIdWritable[] ids) {
        super(DocIdWritable.class, ids);
    }

    /**
     * Factory method.
     *
     * Builds a new object by reading the information from a
     * DataInput.
     *
     * @param in            Data source.
     * @return              Created object.
     * @throws IOException  If the data source couldn't be read
     *                      correctly.
     */
    public static DocIdArrayWritable read(DataInput in) throws IOException {
        DocIdArrayWritable docIdArray = new DocIdArrayWritable();
        docIdArray.readFields(in);
        return docIdArray;
    }

    @Override
    public String[] toStrings() {
        DocIdWritable[] objs = (DocIdWritable [])toArray();
        String[] array = new String[objs.length];
        for (int i=0; i < objs.length; i++)
            array[i] = objs[i].toString();
        return array;
    }

    @Override
    public String toString() {
        Joiner joiner = Joiner.on(" ][ ").skipNulls();
        return joiner.join(this.toStrings());
    }   
}
