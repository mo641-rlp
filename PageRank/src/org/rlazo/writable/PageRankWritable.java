package org.rlazo.writable;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

import org.rlazo.writable.DocIdArrayWritable;
import org.rlazo.writable.DocIdWritable;

import org.apache.hadoop.io.DoubleWritable;
import org.apache.hadoop.io.Writable;

/**
 * Encapsulates the PageRank values of a page.
 *
 * Stores the page's current PageRank value plus all its outgoing
 * links (link graph). This class is used as input of the mapper and
 * output of the reducer in the iterative execution of the PageRank
 * MapReduce.
 *
 * @author Rodrigo Lazo - RA109228
 */
public class PageRankWritable implements Writable {
    public static final double DEFAULT_PR = 0.0d;
    private DoubleWritable pageRank = new DoubleWritable(DEFAULT_PR);
    private DocIdArrayWritable docIds = new DocIdArrayWritable();

    /**
     * Factory method.
     *
     * Builds a new object by reading the information from a
     * DataInput.
     *
     * @param in            Data source.
     * @return              Created object.
     * @throws IOException  If the data source couldn't be read
     *                      correctly.
     */
    public static DocIdWritable read(DataInput in) throws IOException {
        DocIdWritable obj = new DocIdWritable();
        obj.readFields(in);
        return obj;
    }

    public PageRankWritable() {}

    /**
     * Constructor.
     *
     * @param pageRank  Current PageRank value.
     * @param docIds    List of outgoing links.
     */
    public PageRankWritable(Double pageRank, DocIdWritable[] docIds) {
        set(pageRank, docIds);
    }

    /**
     * Sets the object's value, overwriting previous values.
     *
     * @param pageRank  Current PageRank value.
     * @param docIds    List of outgoing links.
     */
    public void set(Double pageRank, DocIdWritable[] docIds) {
        this.pageRank.set(pageRank);
        this.docIds.set(docIds);
    }

    /**
     * Returns the PageRank value, by default 0.0d.
     */
    public double getPageRank() {
        return pageRank.get();
    }

    /**
     * Return an DocIdArrayWritable object storing all the outgoing links.
     *
     * If this value hasn't been set, the object will empty.
     */
    public DocIdWritable[] getOutgoingLinks() {
        return (DocIdWritable[]) docIds.toArray();
    }

    /**
     * Returns the default PageRank value.
     */
    public double getDefaultPageRank() {
        return DEFAULT_PR;
    }

    /**
     * Loads data into the object, overwriting previous values.
     *
     * @param in  Data source
     * @throws IOException  If the source cannot be read.
     */
    @Override
    public void readFields(DataInput in) throws IOException {
        pageRank.readFields(in);
        docIds.readFields(in);
    }

    /**
     * Dumps the object's data.
     *
     * @param out  Data destination
     * @throws IOException  If the destination cannot process the data.
     */
    @Override
    public void write(DataOutput out) throws IOException {
        pageRank.write(out);
        docIds.write(out);
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof PageRankWritable))
            return false;
        PageRankWritable other = (PageRankWritable)obj;
        return other.docIds.equals(other.docIds);
    }

    @Override
    public int hashCode() {
        return docIds.hashCode();
    }

    @Override
    public String toString() {
        return pageRank.toString() + ": " + docIds.toString();
    }
}
