package org.rlazo.writable;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.WritableComparable;

/**
 * Encapsulates the representation of a document's ID.
 *
 * A document contains a raw document identifier (URI), this class
 * wraps that ID so it's more efficient to process during MapReduce
 * executions.
 *
 * @author Rodrigo Lazo - RA109228
 */
public class DocIdWritable implements WritableComparable<DocIdWritable> {
    private Text uri = new Text();

    /**
     * Factory method.
     *
     * Builds a new object by reading the information from a
     * DataInput.
     *
     * @param in            Data source.
     * @return              Created object.
     * @throws IOException  If the data source couldn't be read
     *                      correctly.
     */
    public static DocIdWritable read(DataInput in) throws IOException {
        DocIdWritable obj = new DocIdWritable();
        obj.readFields(in);
        return obj;
    }

    public DocIdWritable() {}

    /**
     * Constructor.
     *
     * @param uri  Document identifier as extracted from the raw_data.
     */
    public DocIdWritable(String uri) {
        set(uri);
    }

    /**
     * Sets the object's value, overwritting previous values.
     *
     * @param uri  Document identifier as extracted from the raw_data.
     */
    public void set(String uri) {
        this.uri.set(uri);
    }

    /**
     * Returns the raw document identifier (URI).
     */
    public String getURI() {
        return uri.toString();
    }

    /**
     * Loads data into the object, overwriting previous values.
     *
     * @param in  Data source
     * @throws IOException  If the source cannot be read.
     */
    @Override
    public void readFields(DataInput in) throws IOException {
        uri.readFields(in);
    }

    /**
     * Dumps the object's data.
     *
     * @param out  Data destination
     * @throws IOException  If the destination cannot process the data.
     */
    @Override
    public void write(DataOutput out) throws IOException {
        uri.write(out);
    }

    @Override
    public int compareTo(DocIdWritable obj) {
        return uri.compareTo(obj.uri);
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof DocIdWritable))
            return false;
        DocIdWritable other = (DocIdWritable)obj;
        return uri.equals(other.uri);
    }

    @Override
    public int hashCode() {
        return uri.hashCode();
    }

    @Override
    public String toString() {
        return uri.toString();
    }
}
