package org.rlazo.writable;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

import org.rlazo.writable.PageRankWritable;

import org.apache.hadoop.io.BooleanWritable;
import org.apache.hadoop.io.DoubleWritable;
import org.apache.hadoop.io.Writable;

/**
 * Encapsulates the intermediate results of a PageRank MapReduce execution.
 *
 * Stores the data to be send from the mapper to the reducer in the
 * iterative phase of a PageRank computation. This class can be seen
 * as an union, which supports two types of information. First, it can
 * store the PR/C values that a page emits to all its outgoing
 * neighbors. Second, it can store the original PageRankWritable
 * object, so this information could be send to the corresponding
 * reducer, thus preserving the link graph.
 *
 * @author Rodrigo Lazo - RA109228
 */
public class PageRankIntermediateWritable implements Writable {
    private static final double DEFAULT_PR_C = 0.0d;
    private DoubleWritable pr_c = new DoubleWritable(DEFAULT_PR_C);
    private BooleanWritable referenceExists = new BooleanWritable(false);
    private PageRankWritable reference = null;

    /**
     * Factory method.
     *
     * Builds a new object by reading the information from a
     * DataInput.
     *
     * @param in            Data source.
     * @return              Created object.
     * @throws IOException  If the data source couldn't be read
     *                      correctly.
     */
    public static DocIdWritable read(DataInput in) throws IOException {
        DocIdWritable obj = new DocIdWritable();
        obj.readFields(in);
        return obj;
    }

    public PageRankIntermediateWritable() {}

    /**
     * Constructor.
     *
     * Use this constructor when using the object to store the
     * PageRank/Count ratio.
     *
     * @param pr_c  PageRank/Count ratio.
     * @throws InvalidPageRankRatioException 
     */
    public PageRankIntermediateWritable(Double pr_c) throws InvalidPageRankRatioException {
        set(pr_c, null);
    }

    /**
     * Constructor.
     *
     * Use this constructor when using the object to store the
     * original PageRankWritable object.
     *
     * @param reference  Reference to the page's PageRankWritable object.
     * @throws InvalidPageRankRatioException 
     */
    public PageRankIntermediateWritable(PageRankWritable reference) throws InvalidPageRankRatioException {
        set(DEFAULT_PR_C, reference);
    }

    /**
     * Sets the object's value, overwriting previous values.
     *
     * @param pr_c  PageRank/Count ratio.
     * @param reference  Reference to the page's PageRankWritable object.
     * @throws InvalidPageRankRatioException 
     */
    public void set(Double pr_c, PageRankWritable reference) throws InvalidPageRankRatioException {
        if (pr_c < 0) {
            throw new InvalidPageRankRatioException();
        }
        this.pr_c.set(pr_c);
        if (reference != null) {
            this.reference = reference;
            this.referenceExists.set(true);
        }
    }

    /**
     * Returns the PageRankWritable reference, or null if not present.
     */
    public PageRankWritable getPageRankWritable() {
        return reference;
    }

    /**
     * Returns the default PageRank/Counter ratio.
     */
    public double getDefaultPageRankRatio() {
        return DEFAULT_PR_C;
    }

    /**
     * Return the PageRank/Counter ratio.
     *
     * If the ratio has not been specified, this returns the same
     * value as getDefaultPageRankRatio()
     */
    public double getPageRankCounterRatio() {
	return pr_c.get();
    }

    /**
     * Loads data into the object, overwriting previous values.
     *
     * @param in  Data source
     * @throws IOException  If the source cannot be read.
     */
    @Override
    public void readFields(DataInput in) throws IOException {
        pr_c.readFields(in);
        referenceExists.readFields(in);
        if (referenceExists.get()) {
            reference = new PageRankWritable();
            reference.readFields(in);
        } else {
            reference = null;
        }
    }

    /**
     * Dumps the object's data.
     *
     * @param out  Data destination
     * @throws IOException  If the destination cannot process the data.
     */
    @Override
    public void write(DataOutput out) throws IOException {
        pr_c.write(out);
        referenceExists.write(out);
        if (referenceExists.get()) {
            reference.write(out);
        }
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof PageRankIntermediateWritable))
            return false;
        PageRankIntermediateWritable other = (PageRankIntermediateWritable)obj;
        if (!referenceExists.get() && !other.referenceExists.get()) {
            return pr_c.equals(other.pr_c);
        }
        else if (!referenceExists.get() || !other.referenceExists.get()) {
            return false;
        }
        return reference.equals(other.reference);
    }

    @Override
    public int hashCode() {
        if (referenceExists.get())
            return reference.hashCode();
        return pr_c.hashCode();
    }

    @Override
    public String toString() {
        String output = pr_c.toString();
        if (referenceExists.get()) {
            output += ": " + reference.toString();
        }
        return output;
    }
}
