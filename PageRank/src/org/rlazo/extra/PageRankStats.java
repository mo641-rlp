package org.rlazo.extra;

import java.io.File;
import java.io.IOException;
import java.io.PrintStream;
import java.util.Collection;
import java.util.Collections;
import java.util.Map;
import java.util.SortedMap;

import org.apache.log4j.Logger;

import com.google.common.base.Function;
import com.google.common.base.Functions;
import com.google.common.collect.Maps;

import edu.uci.ics.jung.graph.DirectedGraph;

public class PageRankStats {
    private static final int max_edges_shown = -1;
    private PageRankGraph pageRankgraph;
    private static final Logger logger = Logger.getLogger(PageRankVisualizer.class);
    
    public PageRankStats(String inputPath) throws IOException {
        pageRankgraph = new PageRankGraph(inputPath, max_edges_shown);
    }
    
    public void printMap(Map<Integer, Integer> map, PrintStream stream) {
        for (Map.Entry<Integer, Integer> e: map.entrySet()) {
            stream.format("%d \t %d\n", e.getKey(), e.getValue());
        }
    }
    
    public <K> SortedMap<Integer, Integer> buildSortedMapOfdegrees(Collection<K> vertices, 
            Function<K, Integer> function) {
        SortedMap<Integer, Integer> map = Maps.newTreeMap();
        Function<Integer, Integer> lookupFunction = Functions.forMap(map, 0);
        for (K vertex: vertices) {
            Integer degree = function.apply(vertex);
            map.put(degree, lookupFunction.apply(degree).intValue() + 1);
        }
        return map;
    }
    
    public void report(PrintStream stream) throws IOException {
        final DirectedGraph<String, Double> graph = pageRankgraph.getGraph();
        stream.println("Vertex and Edges");
        stream.println("----------------");
        stream.format("Edges: %d\n", graph.getEdgeCount());
        stream.format("Vertices: %d\n", graph.getVertexCount());
        SortedMap<Integer, Integer> inDegrees = buildSortedMapOfdegrees(graph.getVertices(), 
                new Function<String, Integer>() {
           @Override 
           public Integer apply(String vertexName) {
               return graph.getInEdges(vertexName).size();
           }
        });        
        SortedMap<Integer, Integer> outDegrees = buildSortedMapOfdegrees(graph.getVertices(), 
                new Function<String, Integer>() {
            @Override 
            public Integer apply(String vertexName) {
                return graph.getOutEdges(vertexName).size();
            }
         });        
        stream.format("Sources: %d\n", inDegrees.get(0));
        stream.format("Sinks: %d\n", outDegrees.get(0));
        int max_in = Collections.max(inDegrees.keySet());
        int max_out = Collections.max(outDegrees.keySet());
        stream.format("Greatest in-degree: %d (%d)\n", max_in, inDegrees.get(max_in));
        stream.format("Greatest out-degree: %d (%d)\n", max_out, outDegrees.get(max_out));
        stream.println("List of IN degrees\n----------");
        printMap(inDegrees, stream);
        stream.println("\nList of OUT degrees\n----------");
        printMap(outDegrees, stream);
    }
    
    public void htmlReport(PrintStream stream) throws IOException {
        DirectedGraph<String, Double> graph = pageRankgraph.getGraph();
        stream.println("<html><head></head><body>");
        
        for (String vertex: graph.getVertices()) {
            stream.format("<p><span id=\"%s\"><strong>%s</strong></span>: ", vertex, vertex);
            for (String neigbor: graph.getSuccessors(vertex)) {
                stream.format("<a href=\"#%s\">%s</a>, ", neigbor, neigbor);
            }
            stream.println("</p>");
        }        
        stream.println("</html>\n");
    }

    public static void main(String[] args) throws IOException {
        if (args.length != 1) {
            logger.error("Missing SequecenFile path.");
            System.exit(-1);
        }
        PageRankStats stats = new PageRankStats(args[0]);
        stats.report(System.out);
    }
}///
