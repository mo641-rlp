package org.rlazo.extra;

import java.awt.Color;
import java.awt.Dimension;
import java.io.IOException;

import javax.swing.JFrame;

import org.apache.log4j.Logger;

import edu.uci.ics.jung.algorithms.layout.ISOMLayout;
import edu.uci.ics.jung.algorithms.layout.Layout;
import edu.uci.ics.jung.visualization.VisualizationViewer;
import edu.uci.ics.jung.visualization.control.DefaultModalGraphMouse;
import edu.uci.ics.jung.visualization.control.ModalGraphMouse;
import edu.uci.ics.jung.visualization.decorators.ToStringLabeller;

public class PageRankVisualizer {
    private static final int max_edges_shown = 2000;
    private PageRankGraph pageRankgraph;
    private static final Logger logger = Logger.getLogger(PageRankVisualizer.class);
    
    public PageRankVisualizer(String inputPath) throws IOException {
        pageRankgraph = new PageRankGraph(inputPath, max_edges_shown);
    }
    
    public void visualizate() throws IOException {
        Layout<String, Double> layout = new ISOMLayout<String, Double>(pageRankgraph.getGraph());
        layout.setSize(new Dimension(4000, 4000));
        VisualizationViewer<String, Double> vv = 
            new VisualizationViewer<String, Double>(layout);
        vv.setPreferredSize(new Dimension(850, 850));
        vv.getRenderContext().setVertexLabelTransformer(new ToStringLabeller<String>());
        DefaultModalGraphMouse<String, Double> gm = new DefaultModalGraphMouse<String, Double>();
        gm.setMode(ModalGraphMouse.Mode.TRANSFORMING);
        vv.setGraphMouse(gm);
        vv.setBackground(Color.WHITE);
        
        JFrame frame = new JFrame("PageRank visualizator");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.getContentPane().add(vv);
        frame.pack();
        logger.warn("listo");
        frame.setVisible(true);        
    }

    /**
     * @param args
     * @throws IOException 
     */
    public static void main(String[] args) throws IOException {
        if (args.length != 1) {
            logger.error("Missing path argument.");
            System.exit(-1);
        }
        PageRankVisualizer visualizer = new PageRankVisualizer(args[0]);
        visualizer.visualizate();
    }
}
