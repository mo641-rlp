package org.rlazo.extra;

import java.io.IOException;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.SequenceFile;
import org.apache.log4j.Logger;
import org.rlazo.writable.DocIdWritable;
import org.rlazo.writable.PageRankWritable;

import edu.uci.ics.jung.graph.DirectedGraph;
import edu.uci.ics.jung.graph.DirectedSparseGraph;
import edu.uci.ics.jung.graph.util.EdgeType;

public class PageRankGraph {
    private Path inputPath;
    private FileSystem fs;
    private int max_edges;
    private Configuration configuration = new Configuration();   
    private DirectedGraph<String, Double> graph = null;    
    private static final Logger logger = Logger.getLogger(PageRankGraph.class);
    
    public PageRankGraph(String inputPath, int max_edges) throws IOException {
        fs = FileSystem.get(configuration);        
        this.inputPath = new Path(inputPath);    
        if (!fs.exists(this.inputPath)) {
            logger.error("Input path does not exists.");
            System.exit(-1);
        }
        this.max_edges = max_edges < 0 ? Integer.MAX_VALUE : max_edges;
    }
    
    public DirectedGraph<String, Double> getGraph() throws IOException {
        if (graph == null) {
            buildGraph();
        }
        return graph;
    }
   
    private void buildGraph() throws IOException {
        logger.info("building graph");
        graph = new DirectedSparseGraph<String, Double>();
        SequenceFile.Reader reader =  new SequenceFile.Reader(fs, inputPath, configuration);
        DocIdWritable key = new DocIdWritable();
        PageRankWritable value = new PageRankWritable();        
        double counter = 0.0d;
        while (reader.next(key, value) && max_edges - counter > 0) {            
            for (DocIdWritable docId: value.getOutgoingLinks()) {
                graph.addEdge(counter++, key.getURI(), docId.getURI(), EdgeType.DIRECTED);                
            }            
        }  
        logger.info("Done building graph.");
    }
}
