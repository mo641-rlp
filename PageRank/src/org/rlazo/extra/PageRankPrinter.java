package org.rlazo.extra;

import java.io.IOException;
import java.io.PrintStream;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.SequenceFile;
import org.rlazo.writable.DocIdWritable;
import org.rlazo.writable.PageRankWritable;

import com.google.common.base.Preconditions;

public class PageRankPrinter {
    public static void main(String[] args) throws IOException {
        Preconditions.checkArgument(args.length == 2);
        Configuration conf = new Configuration();
        FileSystem fs = FileSystem.get(conf);
        SequenceFile.Reader reader = new SequenceFile.Reader(fs, new Path(args[0]),
                                                             conf);
        DocIdWritable key = new DocIdWritable();
        PageRankWritable value = new PageRankWritable();
        PrintStream output = new PrintStream(args[1]);
        while (reader.next(key, value)) {
            output.format("%s [%.12f] > ", key.getURI(), value.getPageRank());
            for (DocIdWritable neighbor: value.getOutgoingLinks()) {
                output.print(neighbor.getURI() + " - ");
            }
            output.println("");
        }
        reader.close();
    }
}