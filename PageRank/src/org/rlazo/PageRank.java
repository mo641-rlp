package org.rlazo;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.log4j.Logger;
import org.rlazo.mapreduce.CleanUpMapReduce;
import org.rlazo.mapreduce.LinkGraphGeneratorMapReduce;
import org.rlazo.mapreduce.PageRankEstimatorMapReduce;
import org.rlazo.mapreduce.conf.ConfigurationLoader;

public class PageRank {
    private static final Logger logger = Logger.getLogger(PageRank.class);

    public static void main(String[] args) throws Exception {
        Configuration conf = new ConfigurationLoader("pagerank").loadFromArgs(args);

        FileSystem fs = FileSystem.get(conf);

        if (conf.getBoolean("pagerank.mapreduce.phaseOne.run", true)) {
            if (LinkGraphGeneratorMapReduce.run(args, conf) != 0) {
                die("First phase failed. Stopping.");
            }
        } else {
            logger.info("Skipping first phase.");
        }
        if (conf.getBoolean("pagerank.mapreduce.phaseTwo.run", true)) {
            conf = PageRankEstimatorMapReduce.run(args, conf);
            Path phaseTwoOutputPath = new Path(conf.get(
                    "pagerank.mapreduce.phaseTwo.outputPath"));
            Path phaseThreeInputPath = new Path(conf.get(
                    "pagerank.mapreduce.phaseThree.inputPath"));
            if (!phaseTwoOutputPath.equals(phaseThreeInputPath)) {
                if (fs.exists(phaseThreeInputPath)) {
                    fs.delete(phaseThreeInputPath, true);
                }
                fs.rename(phaseTwoOutputPath, phaseThreeInputPath);
            }
        } else {
            logger.info("Skipping second phase.");
        }
        if (conf.getBoolean("pagerank.mapreduce.phaseThree.run", true)) {
            if (CleanUpMapReduce.run(args, conf) != 0) {
                die("Second phase failed. Stopping.");
            }
        } else {
            logger.info("Skipping third phase.");
        }
    }

    private static void die(String message) {
        PageRank.logger.fatal(message);
        System.exit(-1);
    }
}
