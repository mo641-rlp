package org.rlazo.wikipedia;

import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import javax.xml.stream.events.XMLEvent;

import de.tudarmstadt.ukp.wikipedia.parser.Link;
import de.tudarmstadt.ukp.wikipedia.parser.ParsedPage;
import de.tudarmstadt.ukp.wikipedia.parser.mediawiki.MediaWikiParser;
import de.tudarmstadt.ukp.wikipedia.parser.mediawiki.MediaWikiParserFactory;

/**
 * Parses Wikipedia articles.
 *
 * This class process Wikipedia articles which are wrapped in some XML
 * but their contents are encoded using Wikipedia's markup. The goal
 * here is provide a simple and efficient high-level API, in case you
 * need a more complete parser for Wikipedia take a look at
 * de.tudarmstadt.ukp.wikipedia.parser
 *
 * @author Rodrigo Lazo - RA109228
 */
public class WikipediaParser {
    private static final String TITLE_TAG = "title";
    private static final String CONTENT_TAG = "text";
    private static final Pattern XML_DOCUMENT_TAG_PATTERN =
        Pattern.compile("^\\s*<xml>.+?</xml>\\s*$");
    private static final Pattern EXTRA_UNDERSCORES_PATTERN = Pattern.compile("__*");
    private static final MediaWikiParserFactory MEDIAWIKI_FACTORY = new MediaWikiParserFactory();
    private static final XMLInputFactory XML_FACTORY = XMLInputFactory.newInstance();

    /**
     * Parses a XML string into a WikipediaPage.
     *
     * This method examines the text and extracts from it some
     * information, creating a WikipediaPage. If the XML is bad
     * formatted, an exception is thrown, but if the XML does not
     * contains enough information to build a WikipediaPage, null is
     * returned instead.
     *
     * @param text  String of text to parse.
     * @returns A WikipediaPage object storing the parsed text, or
     *          null if parsing was unsuccessful.
     * @throws XMLStreamException  If text has syntactical errors.
     */
    public WikipediaPage parse(String text) throws XMLStreamException {
        String title = null;
        String content = null;
        XMLStreamReader reader = XML_FACTORY.createXMLStreamReader(
            new StringReader(normalizeXML(text)));
        while (reader.hasNext()) {
            if ( reader.next() == XMLEvent.START_ELEMENT) {
                if (reader.getLocalName() == TITLE_TAG) {
                    title = reader.getElementText();
                } else if (reader.getLocalName() == CONTENT_TAG) {
                    content = reader.getElementText();
                }
            }
        }
        if (title == null) return null;
        WikipediaPage.Builder builder = new WikipediaPage.Builder(normalizeTitle(title));
        builder.extendLinks(extractInternalLinks(content));
        return builder.build();
    }

    /**
     * Verifies and fixes the text so it complies with XML format.
     *
     * The features verified are very basic, and modifications are
     * minimal. The string returned from this method is not guaranteed
     * to be valid-XML, only simple, known clean-ups that may occur
     * in wikipedia articles are performed.
     *
     * @param text  XML string.
     * @returns A fixed version of the input XML string.
     */
    protected String normalizeXML(String text) {
        if (XML_DOCUMENT_TAG_PATTERN.matcher(text).find())
            return text;
        return String.format("<xml>%s</xml>", text);
    }

    /**
     * Modifies the title so it uses Wikipedia's canonical format.
     *
     * @param title  String storing the title to normalize.
     * @returns A normalized title string.
     */
    protected String normalizeTitle(String title) {
        MediaWikiParser parser = MEDIAWIKI_FACTORY.createParser();
        ParsedPage page = parser.parse(String.format("[[%s]]", title));
        return EXTRA_UNDERSCORES_PATTERN.matcher(
                page.getLinks().get(0).getTarget()).replaceAll("_");
    }

    /**
     * Extracts the links whose target is another wikipedia page.
     *
     * @param text  String with wikipedia's markup to analyze.
     * @return List of links' targets.
     */
    protected List<String> extractInternalLinks(String text) {
        List<String> links = new ArrayList<String>();
        ParsedPage page = MEDIAWIKI_FACTORY.createParser().parse(text);
        if (page != null) {
            for (Link link: page.getLinks()) {
                if (link.getType() == Link.type.INTERNAL) {
                    links.add(link.getTarget());
                }
            }
        }
        return links;
    }
}
