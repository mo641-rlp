package org.rlazo.wikipedia;

import java.util.List;

import com.google.common.collect.ImmutableList;

/**
 * Writable array of document's ids.
 *
 * Simple wrapper around ArrayWritable to make it support
 * DocIdWritable objects. If when created no array is provided, a
 * 0-length array is assumed for validity purposes.
 *
 * @author Rodrigo Lazo - RA109228
 */
public final class WikipediaPage {
    private final String title;
    private final List<String> links;

    public static class Builder {
        // Required parameters
        private final String title;
        private ImmutableList.Builder<String> links_builder = ImmutableList.builder();

        // Optional parameters
        public Builder(String title) {
            this.title = title;
        }

        public Builder appendLink(String link) {
            this.links_builder.add(link);
            return this;
        }

        public Builder extendLinks(List<String> links) {
            this.links_builder.addAll(links);
            return this;
        }

        public WikipediaPage build() {
            return new WikipediaPage(this);
        }
    }

    private WikipediaPage(Builder builder) {
        title = builder.title;
        links = builder.links_builder.build();
    }

    public String getTitle() {
        return title;
    }

    public List<String> getLinks() {
        return links;
    }

    public static void main(String[] args) {
        WikipediaPage page = new WikipediaPage.Builder("pagina").appendLink("milink").build();
        page.getLinks().add("s");
        for(String e: page.getLinks()) {
            System.out.println(e);
        }
    }
}