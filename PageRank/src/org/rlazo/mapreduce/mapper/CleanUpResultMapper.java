package org.rlazo.mapreduce.mapper;

import java.io.IOException;

import org.apache.hadoop.io.DoubleWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;
import org.rlazo.writable.DocIdWritable;
import org.rlazo.writable.PageRankWritable;

/**
 * Clean up Mapper.
 *
 * Removes the extra information stored in the input data, so the only
 * left is the page's ID and its pageRank.
 *
 * @author Rodrigo Lazo - RA109228
 */
public class CleanUpResultMapper extends
        Mapper<DocIdWritable, PageRankWritable, DoubleWritable, Text> {
    /**
     * A multiplier to apply to every PageRank value so the output is more readable.
     */
    private static double multiplier = 1000;

    @Override
    protected void map(DocIdWritable key, PageRankWritable value, Context context)
            throws IOException, InterruptedException {
        context.write(new DoubleWritable(value.getPageRank() * multiplier),
                new Text(key.getURI()));
    }

    public static void setMultiplier(double value) {
       multiplier = value;
    }

    public static double getMultiplier() {
        return multiplier;
    }
}
