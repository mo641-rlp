package org.rlazo.mapreduce.mapper;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.xml.stream.XMLStreamException;

import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;
import org.rlazo.wikipedia.WikipediaPage;
import org.rlazo.wikipedia.WikipediaParser;
import org.rlazo.writable.DocIdWritable;
import org.rlazo.writable.PageRankWritable;

/**
 * Mapper that extracts the link structure from Wikipedia pages.
 *
 * This mapper parses Wikipedia pages and extracts the pages ID plus a
 * list of all its outgoing links. It returns a PageRankWritable using
 * a default PageRank value.
 *
 * It contains the following counter:
 *  - XML_PARSING_ERRORS: number of errors found while parsing the page
 *  - SINKS: number of pages that have no outgoing links.
 *
 * @author Rodrigo Lazo - RA109228
 */
public class LinkGraphGeneratorMapper extends
        Mapper<LongWritable, Text, DocIdWritable, PageRankWritable> {
    public static enum LinkGraphCreatorCounters { XML_PARSING_ERRORS, SINKS };

    private WikipediaParser parser = new WikipediaParser();
    private static double initial_pagerank = 0.000001d;

    @Override
    protected void map(LongWritable key, Text value, Context context)
            throws IOException, InterruptedException {
        try {
            WikipediaPage page = parser.parse(value.toString());
            DocIdWritable output_key = new DocIdWritable(page.getTitle());
            List<DocIdWritable> link_list = new ArrayList<DocIdWritable>();
            if (page.getLinks().size() == 0) {  // Number of pages without out-links
                context.getCounter(LinkGraphCreatorCounters.SINKS).increment(1);
            }
            for (String link: page.getLinks()) {
                link_list.add(new DocIdWritable(link));
            }
            DocIdWritable[] link_array = new DocIdWritable[link_list.size()];
            int i = 0;
            for (DocIdWritable obj: link_list) {
                link_array[i++] = obj;
            }
            PageRankWritable output_value = new PageRankWritable(initial_pagerank, link_array);
            context.write(output_key, output_value);
        } catch (XMLStreamException e) {
            context.getCounter(LinkGraphCreatorCounters.XML_PARSING_ERRORS).increment(1);
        }
    }

    public static void setDefaultPageRank(double value) {
        initial_pagerank = value;
    }
    public static double getDefaultPageRank() {
        return initial_pagerank;
    }
}
