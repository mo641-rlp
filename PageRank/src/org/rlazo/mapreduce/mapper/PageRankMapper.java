package org.rlazo.mapreduce.mapper;

import java.io.IOException;

import org.apache.hadoop.mapreduce.Mapper;
import org.rlazo.writable.DocIdWritable;
import org.rlazo.writable.InvalidPageRankRatioException;
import org.rlazo.writable.PageRankIntermediateWritable;
import org.rlazo.writable.PageRankWritable;

/**
 * Distributes the PageRank value of a page to its neighbors.
 *
 * Computes the ratio PageRank/NoLinks and sends that information to
 * its neighbors. Also, it emits the PageRankWritable reference it
 * gets to the reducer, so the link structure is not lost.
 *
 * It contains the following counter:
 *  - MAPPER_ERRORS: number of invalid PageRank values.
 *
 * @author Rodrigo Lazo - RA109228
 */
public class PageRankMapper extends
Mapper<DocIdWritable, PageRankWritable, DocIdWritable, PageRankIntermediateWritable> {
    public static enum PageRankMapperCounter {MAPPER_ERRORS};

    @Override
    protected void map(DocIdWritable key, PageRankWritable value, Context context) throws
        IOException, InterruptedException {
        try {
            double pr_c = value.getPageRank();
            if (value.getOutgoingLinks().length > 0)
                pr_c /= value.getOutgoingLinks().length;
            // TODO: delete these statements
            // System.out.print(value.getPageRank() + " ");
            // System.out.println(pr_c);
            for (DocIdWritable neighbor: value.getOutgoingLinks()) {
                context.write(neighbor, new PageRankIntermediateWritable(pr_c));
            }
            context.write(key, new PageRankIntermediateWritable(value));  // emit link graph
        } catch (InvalidPageRankRatioException e) {
            e.printStackTrace();
            context.getCounter(PageRankMapperCounter.MAPPER_ERRORS).increment(1);
        }
    }

}
