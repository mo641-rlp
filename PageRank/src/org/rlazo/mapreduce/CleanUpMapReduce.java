package org.rlazo.mapreduce;

import java.io.IOException;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.DoubleWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.SequenceFileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;
import org.apache.log4j.Logger;
import org.rlazo.mapreduce.mapper.CleanUpResultMapper;

public class CleanUpMapReduce {
    private static final Logger logger = Logger.getLogger(
        CleanUpMapReduce.class);

    private static class Runner extends Configured implements Tool {
        private Job job = null;

        @Override
        public int run(String[] args) throws Exception{
            job = new Job(getConf(), "Clean up step");
            job.setJarByClass(CleanUpMapReduce.class);
            loadConfiguration(job);

            job.setMapperClass(CleanUpResultMapper.class);
            job.setReducerClass(Reducer.class);
            
            job.setMapOutputKeyClass(DoubleWritable.class);
            job.setMapOutputValueClass(Text.class);

            job.setInputFormatClass(SequenceFileInputFormat.class);
            job.setOutputFormatClass(TextOutputFormat.class);
            job.setOutputKeyClass(DoubleWritable.class);
            job.setOutputValueClass(Text.class);

            job.waitForCompletion(true);
            return 0;
        }

        private void loadConfiguration(Job job) throws IOException {
            Configuration conf = job.getConfiguration();

            CleanUpResultMapper.setMultiplier(
                    conf.getFloat("pagerank.mapreduce.FinalMultiplier", 1));
            job.setNumReduceTasks(
                    conf.getInt("pagerank.mapreduce.phaseThree.numReducers", 1));
            FileInputFormat.addInputPath(job,
                    new Path(conf.get("pagerank.mapreduce.phaseThree.inputPath",
                            "phase2output")));
            FileOutputFormat.setCompressOutput(job,
                    conf.getBoolean("pagerank.mapreduce.phaseThree.compressedOutput", false));
            FileOutputFormat.setOutputPath(job,
                    new Path(conf.get("pagerank.mapreduce.phaseThree.outputPath", 
                            "finaloutput")));
        }
    }

    public static int run(String args[], Configuration conf) throws Exception {
        logger.info("Clean up starting.");
        int res = ToolRunner.run(conf, new Runner(), args);
        return res;
    }
}