package org.rlazo.mapreduce;

import java.io.IOException;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.SequenceFileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.SequenceFileOutputFormat;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;
import org.apache.log4j.Logger;
import org.rlazo.mapreduce.mapper.PageRankMapper;
import org.rlazo.mapreduce.reducer.PageRankReducer;
import org.rlazo.mapreduce.reducer.PageRankReducer.PageRankReducerCounter;
import org.rlazo.writable.DocIdWritable;
import org.rlazo.writable.PageRankIntermediateWritable;
import org.rlazo.writable.PageRankWritable;

public class PageRankEstimatorMapReduce {
    private static final Logger logger = Logger.getLogger(
        PageRankEstimatorMapReduce.class);

    private static class Runner extends Configured implements Tool {
        private Job job = null;
        private FileSystem fs;        
        private long counter = 0;
        
        public Runner() throws IOException {
            fs = FileSystem.get(new Configuration());
        }

        @Override
        public int run(String[] args) throws Exception{
            job = new Job(getConf(), "PageRank Estimator");
            job.setJarByClass(Runner.class);
            loadConfiguration(job);

            job.setMapperClass(PageRankMapper.class);
            job.setReducerClass(PageRankReducer.class);

            job.setInputFormatClass(SequenceFileInputFormat.class);
            job.setOutputFormatClass(SequenceFileOutputFormat.class);
            job.setMapOutputKeyClass(DocIdWritable.class);
            job.setMapOutputValueClass(PageRankIntermediateWritable.class);
            job.setOutputKeyClass(DocIdWritable.class);
            job.setOutputValueClass(PageRankWritable.class);

            job.waitForCompletion(true);
            counter = job.getCounters().findCounter(
                           PageRankReducerCounter.PR_VARIATION_NOT_WITHIN_E).getValue();
            return 0;
        }
        
        public long getCounter() {
            return counter;
        }

        private void loadConfiguration(Job job) throws IOException {
            Configuration conf = job.getConfiguration();
            PageRankReducer.setD(conf.getFloat("pagerank.mapreduce.D", 0.85f));
            PageRankReducer.setEpsilon(conf.getFloat("pagerank.mapreduce.epsilon", 0.0001f));
            job.setNumReduceTasks(
                    conf.getInt("pagerank.mapreduce.phaseTwo.numReducers", 5));
            Path inputPath = new Path(conf.get("pagerank.mapreduce.phaseTwo.inputPath",
                    "phase2input"));
            Path outputPath = new Path(conf.get("pagerank.mapreduce.phaseTwo.outputPath",
                    "phase2output"));

            // Allow the iterative execution of the job, by swapping the input and output paths
            if (fs.exists(outputPath)) {
                fs.delete(inputPath, true);
                Path tmp = inputPath;
                inputPath = outputPath;
                outputPath = tmp;
                conf.set("pagerank.mapreduce.phaseTwo.inputPath",
                         inputPath.toUri().getPath());
                conf.set("pagerank.mapreduce.phaseTwo.outputPath",
                         outputPath.toUri().getPath());
            }

            FileInputFormat.addInputPath(job, inputPath);
            FileOutputFormat.setCompressOutput(job,
                    conf.getBoolean("pagerank.mapreduce.phaseTwo.compressedOutput", true));
            FileOutputFormat.setOutputPath(job, outputPath);
        }
    }

    public static Configuration run(String args[], Configuration conf) throws Exception {
        logger.info("PageRank estimator starting.");
        int phaseTwoRounds = conf.getInt("pagerank.mapreduce.phaseTwo.numRounds", 4);        
        Runner runner = new PageRankEstimatorMapReduce.Runner();
        int res = 0;
        for (int i = 0; i < phaseTwoRounds && res == 0; i++) {            
            logger.info("Second Phase - round " + Integer.toString(i+1));
            res =  ToolRunner.run(conf, runner, args);
            logger.info("EPSILON = " + runner.getCounter());
        }
        return runner.job.getConfiguration();
    }
}