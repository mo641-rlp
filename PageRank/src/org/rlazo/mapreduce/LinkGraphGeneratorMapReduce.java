package org.rlazo.mapreduce;

import java.io.IOException;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.SequenceFileOutputFormat;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;
import org.apache.log4j.Logger;
import org.rlazo.mapreduce.mapper.LinkGraphGeneratorMapper;
import org.rlazo.writable.DocIdWritable;
import org.rlazo.writable.PageRankWritable;

public class LinkGraphGeneratorMapReduce {
    private static final Logger logger = Logger.getLogger(
        LinkGraphGeneratorMapReduce.class);

    private static class Runner extends Configured implements Tool {
        private Job job = null;

        @Override
        public int run(String[] args) throws Exception{
            job = new Job(getConf(), "Link Graph Generator");
            job.setJarByClass(LinkGraphGeneratorMapReduce.class);
            loadConfiguration(job);

            job.setMapperClass(LinkGraphGeneratorMapper.class);
            job.setReducerClass(Reducer.class);

            job.setInputFormatClass(TextInputFormat.class);
            job.setOutputFormatClass(SequenceFileOutputFormat.class);
            job.setOutputKeyClass(DocIdWritable.class);
            job.setOutputValueClass(PageRankWritable.class);

            job.waitForCompletion(true);
            return 0;
        }

        private void loadConfiguration(Job job) throws IOException {
            Configuration conf = job.getConfiguration();
            LinkGraphGeneratorMapper.setDefaultPageRank(
                    conf.getFloat("pagerank.mapreduce.initialPagerank", 0.0000001f));
            job.setNumReduceTasks(
                    conf.getInt("pagerank.mapreduce.phaseOne.numReducers", 5));
            FileInputFormat.addInputPath(job,
                    new Path(conf.get("pagerank.mapreduce.phaseOne.inputPath",
                             "input")));
            FileOutputFormat.setCompressOutput(job,
                    conf.getBoolean("pagerank.mapreduce.phaseOne.compressedOutput", true));
            FileOutputFormat.setOutputPath(job,
                    new Path(conf.get("pagerank.mapreduce.phaseOne.outputPath",
                                      "phase2input")));
        }
    }

    public static int run(String args[], Configuration conf) throws Exception {
        logger.info("Link Graph Generation starting.");
        int res = ToolRunner.run(conf, new Runner(), args);
        return res;
    }
}