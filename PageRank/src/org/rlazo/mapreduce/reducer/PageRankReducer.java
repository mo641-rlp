package org.rlazo.mapreduce.reducer;

import java.io.IOException;

import org.apache.hadoop.mapreduce.Reducer;
import org.rlazo.writable.DocIdWritable;
import org.rlazo.writable.PageRankIntermediateWritable;
import org.rlazo.writable.PageRankWritable;

/**
 * Computes the new PageRank for a given Page.
 *
 * This reducer receives a list of PageRanks/NoLinks ratios from pages
 * pointing to the "key" page. Using these values it computes the new
 * PageRank for the Page. Also, it expects a reference to the original
 * PageRankWritable, which contains the outgoing links from this page
 * and its old PageRank value to update.
 *
 * It contains the following counter:
 * - NO_ORIGINAL_REF_RECEIVED: Number of pages which didn't have a
 *    original PageRankWritable reference. This happens when a page is
 *    only pointed by other pages, and it wasn't included in the
 *    original page set. Therefore this page is a "sink", with no
 *    outgoing links.
 *
 * @author Rodrigo Lazo - RA109228
 */
public class PageRankReducer extends
    Reducer<DocIdWritable, PageRankIntermediateWritable, DocIdWritable, PageRankWritable> {
    /**
     * Default probability of the "crazy surfer" clicking on a link.
     */
    private static double D = 0.85;
    private static double epsilon = 0.0001;
    private static final DocIdWritable[] EMPTY_LINK_LIST;
    public static enum PageRankReducerCounter {NO_ORIGINAL_REF_RECEIVED, 
        PR_VARIATION_NOT_WITHIN_E};

    static {
        EMPTY_LINK_LIST = new DocIdWritable[0];
    }

    @Override
    protected void reduce(DocIdWritable key,
            Iterable<PageRankIntermediateWritable> values, Context context)
            throws IOException, InterruptedException {
        double page_rank_sum = 0.0d;
        PageRankWritable original = null;
        for (PageRankIntermediateWritable value: values) {
            if (value.getPageRankWritable() != null)
                original = value.getPageRankWritable();
            page_rank_sum += value.getPageRankCounterRatio();
        }
        double page_rank = (1-D)/28000 + D * page_rank_sum;
       
        if (original != null) {
            // could compare both new and old PR values
            // System.out.format("\t%f - %f\n", page_rank, original.getPageRank());
            if (Math.abs(page_rank - original.getPageRank()) > epsilon) {
                context.getCounter(PageRankReducerCounter.PR_VARIATION_NOT_WITHIN_E).increment(1);
            }
            original.set(page_rank, original.getOutgoingLinks());
            context.write(key, original);
        } else {
            context.getCounter(PageRankReducerCounter.NO_ORIGINAL_REF_RECEIVED).increment(1);
            context.write(key, new PageRankWritable(page_rank, EMPTY_LINK_LIST));
        }
    }

    public static void setD(double d) {
        D = d;
    }

    public static double getD() {
        return D;
    }
    
    public static void setEpsilon(double value) {
        epsilon = value;
    }
    
    public static double getEpsilon() {
        return epsilon;
    }
}
