package org.rlazo.mapreduce.conf;

import org.apache.hadoop.conf.Configuration;

/**
 * Helper class for configuration loading. 
 *
 * @author Rodrigo Lazo - RA109228
 */
public class ConfigurationLoader {
    private String param_prefix;
    
    /**
     * Constructor.
     * 
     * You need to specify as a parameter which string is the common 
     * prefix all the configuration arguments share. 
     * 
     * @param prefix 
     */
    public ConfigurationLoader(String prefix) {
        param_prefix = prefix;
    }
    
    public Configuration loadFromArgs(String[] args) {
        Configuration conf = new Configuration();        
        for (int i=0; i < (args.length - 1); i++)
        {
            if (args[i].startsWith(param_prefix)) {
                conf.set(args[i], args[i+1]);
                i += 1;
            }
        }
        return conf;
    }
}
