                       README PageRank Project
                       =======================

Author: Rodrigo Lazo - ra109228
Date: 2010-10-16 sáb


1 Como conseguir o projecto
~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  O projecto está armazenado em um repositório git público. Pode ser
  descarregado e compilado com os seguente comandos:


  git clone git://repo.or.cz/mo641-rlp.git
  cd PageRank
  ant dist

  Na pasta dist vai ser criado um arquivo PageRank.jar.

2 Testes
~~~~~~~~~
  O projecto inclui algum testes, para executá-los, faça:


  ant test

  Para gerar um reporte, faça:


  ant junitreport

  Após executar esse comando, o reporte fica no arquivo junit/index.html

3 Estrutura do projecto
~~~~~~~~~~~~~~~~~~~~~~~~
  configuration.xml: Arquivo de configuração do job.
  dependencies: Pasta que armazena as dependências do job.
  resources: Pasta que armazena os arquivos adicionais, por exemplo para os testes.
  scripts: Pasta com os scripts necessários para a execução do job.
  src: Código fonte do projecto.
  tests: Testes.

4 Configuração do job
~~~~~~~~~~~~~~~~~~~~~~
  A configuração do job é definida no arquivo configuration.xml e tem
  o formato dos arquivos de configuração do hadoop (mesmas tags). O
  arquivo contem a definição de varias variáveis e uma pequena
  descrição delas. Você não precisa modificar o código JAVA para
  fazer algumas mudanças na execução da aplicação.

5 O Como executar o job.
~~~~~~~~~~~~~~~~~~~~~~~~~
  O projecto contem um script em Python para a execução do job. Você
  precisa copiar três arquivos a sua instalação do hadoop:

  - dist/PageRank.jar
  - scripts/pagerank.py
  - configuration.xml

  Uma vez que você faça as modificações necessárias ao arquivo
  configuration.xml (tais como paths de entrada e saída), verifique
  que o path do executável do hadoop no script scripts/pagerank.py
  seja correto. Logo, só precisa executar o script:


  ./pagerank.py configuration.xml

6 Resultados intermédios
~~~~~~~~~~~~~~~~~~~~~~~~~
  Os resultados intermédios são armazenados em arquivos SequenceFile,
  que podem o não estar compressos (a configuração tem um parâmetro
  que o controla). Para visualizá-los, faça:


  java -cp PageRank.jar org.rlazo.extra.PageRankPrinter <part-r-xxxxx> <output>

  O comando vai ler o SequenceFile, por exemplo part-r-xxxxx, e
  vai-lo escrever descompresso no arquivo de saída, por exemplo
  output. O arquivo de entrada (part-r-xxxxx) tem que estar no sistema
  de arquivos local, (não hdfs), portanto você precisa que descarregar o
  arquivo manualmente.

  Informação adicional sobre a saída intermédia pode ser obtida com o comando


  java -cp PageRank.jar org.rlazo.extra.PageRankStats <part-r-xxxxx>

  O comando mostra algumas estatísticas do grafo, como números de
  arestas, vértices, fontes, etc; além do valor de graus, de entrada
  e de saída dos elementos do grafo.

7 Resultados Finais
~~~~~~~~~~~~~~~~~~~~
  Os resultados são armazenados na pasta:
  pagerank.mapreduce.phaseThree.outputPath em duas colunas de texto
  plano. A primeira coluna é o valor do PageRank da página, e a
  segunda é o nome da página.

8 Problemas encontrados
~~~~~~~~~~~~~~~~~~~~~~~~

8.1 Configuração
=================
   Não encontrei um jeito fácil de parametrizar o MapReduce, ainda
   quando Hadoop suporta arquivos de configuração adicionais, não
   consegue fazê-lo carregar os dados do arquivo. A solução que
   utilizo é fazer que o script de python leia o arquivo XML, e gere o
   comando que contem a configuração como parâmetros da linha de
   comandos. O código JAVA lê os parâmetros da linha de comandos y
   gera o objeto Configuration.

8.2 Dependências
=================
   Uma das dependências tinha dentro de seu JAR uma copia do Xerces, e
   o hadoop ficava confundido por isso, já que ele também precisa
   dessa liberaria mas pode fazer uso da copia incluída no JDK. Eu tive
   que fazer muitas modificações até que encontrei como eliminar a
   copia adicional do Xerces. O log do git do projecto tem muitos
   commits que tentam solucionar isso.

9 Dependencias
~~~~~~~~~~~~~~~
  Google guava-libraries: Utility methods and classes, [http://code.google.com/p/guava-libraries/]
  Java Wikipedia Library (JWPL): JAR modificado, libreria para
       oparsing of plain text. [http://code.google.com/p/jwpl/]
  Java Uniform Network Framework: Utilizado para obter informação
       adicional sob o grafo [http://jung.sourceforge.net/]
  Junit 4: Testes [http://www.junit.org/]
