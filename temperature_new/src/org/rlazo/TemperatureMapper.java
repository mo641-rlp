package org.rlazo;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import org.apache.hadoop.io.FloatWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

public class TemperatureMapper extends Mapper<LongWritable, Text, Text, FloatWritable> {
    private static final int MISSING = 9999;
    private static final String QUALITY_REGEXP = "[01459]";
    private static final String WORD_DELIMITERS_REGEXP = "[\\s]+";

    @Override
    protected void map(LongWritable key, Text value,Context context)
            throws IOException, InterruptedException {
    	String line = value.toString();
        if (line.isEmpty() || line.startsWith("STN")) return;
        List<String> row = Arrays.asList(value.toString().split(WORD_DELIMITERS_REGEXP));
        String year = row.get(2).substring(0, 4);
        String raw_temperature = row.get(3);
        String quality = row.get(4);
        float airTemperature;
        if (raw_temperature.charAt(0) == '-') {
            airTemperature = Float.parseFloat(raw_temperature.substring(1, raw_temperature.length()));
        } else {
            airTemperature = Float.parseFloat(raw_temperature);
        }
        if (airTemperature != MISSING && quality.matches(QUALITY_REGEXP)) {
            context.write(new Text(year), new FloatWritable(airTemperature));
        }
    }

}
