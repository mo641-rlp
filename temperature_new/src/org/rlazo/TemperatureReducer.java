package org.rlazo;

import java.io.IOException;

import org.apache.hadoop.io.FloatWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

public class TemperatureReducer extends
        Reducer<Text, FloatWritable, Text, FloatWritable> {

    @Override
    protected void reduce(Text key, Iterable<FloatWritable> values, Context context)
            throws IOException, InterruptedException {
        float maxValue = Integer.MIN_VALUE;
        for (FloatWritable value : values) {
          maxValue = Math.max(maxValue, value.get());
        }
        context.write(key, new FloatWritable(maxValue));      
    }
}
