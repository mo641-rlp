
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import example.RutaProtos.Ruta;
import example.RutaProtos.SegmentoCaminho;
import example.RutaProtos.SegmentoCaminho.Coordenada;
import example.RutaProtos.SegmentoCaminho.TipoCaminho;
public class ProtobufTest {

    /**
     * @param args
     * @throws IOException
     */
    public static void main(String[] args) throws IOException {
        Ruta.Builder ruta_builder = Ruta.newBuilder();
        ruta_builder.setId(1);

        SegmentoCaminho.Builder segmento_builder = SegmentoCaminho.newBuilder();
        segmento_builder.setId(5);
        segmento_builder.setNome("Um");
        segmento_builder.setInicio(
                Coordenada.newBuilder().setPosX(20).setPosY(40).build());
        segmento_builder.setFin(
                Coordenada.newBuilder().setPosX(40).setPosY(40).build());
        segmento_builder.setTipo(TipoCaminho.AVENIDA);
        ruta_builder.addSegmentos(segmento_builder.build());

        segmento_builder = SegmentoCaminho.newBuilder();
        segmento_builder.setId(6);
        segmento_builder.setNome("Dois");
        segmento_builder.setInicio(
                Coordenada.newBuilder().setPosX(20).setPosY(20).build());
        segmento_builder.setFin(
                Coordenada.newBuilder().setPosX(40).setPosY(20).build());
        segmento_builder.setTipo(TipoCaminho.ESTRADA);
        ruta_builder.addSegmentos(segmento_builder.build());


        FileOutputStream output = new FileOutputStream(new File("salida.txt"));
        ruta_builder.build().writeTo(output);
        output.close();
    }

}
