
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import org.apache.avro.Schema;
import org.apache.avro.generic.GenericArray;
import org.apache.avro.generic.GenericData;
import org.apache.avro.generic.GenericDatumWriter;
import org.apache.avro.generic.GenericRecord;
import org.apache.avro.io.BinaryEncoder;
import org.apache.avro.io.Encoder;
import org.apache.avro.io.JsonEncoder;
import org.apache.avro.specific.SpecificCompiler;
import org.apache.avro.util.Utf8;

public class AvroTest {
    static final String filename = "example.avpr";
    static final String outputDir = "generated/";

    public static void generateCode() throws IOException {
        SpecificCompiler.compileSchema(new File(filename), new File(outputDir));
    }

    public static void writeRecordJson(GenericRecord record, Schema schema) throws IOException {
        ByteArrayOutputStream bao = new ByteArrayOutputStream();
        GenericDatumWriter w = new GenericDatumWriter(schema);
        Encoder e = new JsonEncoder(schema, bao);
        e.init(new FileOutputStream(new File("test_data.avro")));
        w.write(record, e);
        e.flush();
    }

    public static void writeRecordBinary(GenericRecord record, Schema schema) throws FileNotFoundException, IOException {
        ByteArrayOutputStream bao = new ByteArrayOutputStream();
        GenericDatumWriter w = new GenericDatumWriter(schema);
        Encoder e = new BinaryEncoder(bao);
        e.init(new FileOutputStream(new File("test_data_bin.avro")));
        w.write(record, e);
        e.flush();
    }

    /**
     * @param args
     * @throws IOException
     */
    public static void main(String[] args) throws IOException {
       Schema rutaSchema = Schema.parse(new File(filename));
       Schema segmentosSchema = rutaSchema.getField("segmentos").schema();
       Schema segmentosCaminhoSchema = segmentosSchema.getElementType();
       Schema coordenadasSchema = segmentosCaminhoSchema.getField("inicio").schema();


       GenericRecord ruta = new GenericData.Record(rutaSchema);
       GenericRecord segmentoUm = new GenericData.Record(segmentosCaminhoSchema);
       segmentoUm.put("id", 5);
       segmentoUm.put("nome", new Utf8("Um"));
       GenericRecord pos =  new GenericData.Record(coordenadasSchema);
       pos.put("pos_x", 20L);
       pos.put("pos_y", 40L);
       segmentoUm.put("inicio", pos);
       pos.put("pos_x", 40L);
       pos.put("pos_y", 40L);
       segmentoUm.put("tipo", new GenericData.EnumSymbol("AVENIDA"));
       segmentoUm.put("fin", pos);

       GenericRecord segmentoDois = new GenericData.Record(segmentosCaminhoSchema);
       segmentoDois.put("id", 6);
       segmentoDois.put("nome", new Utf8("Dois"));
       pos.put("pos_x", 20L);
       pos.put("pos_y", 20L);
       segmentoDois.put("inicio", pos);
       pos.put("pos_x", 40L);
       pos.put("pos_y", 20L);
       segmentoDois.put("tipo", new GenericData.EnumSymbol("AVENIDA"));
       segmentoDois.put("fin", pos);

       ruta.put("id", 5);
       GenericArray<GenericRecord> segmentos = new GenericData.Array<GenericRecord>(3, segmentosSchema);
       segmentos.add(segmentoUm);
       segmentos.add(segmentoDois);

       ruta.put("segmentos", segmentos);

       writeRecordJson(ruta, rutaSchema);
       writeRecordBinary(ruta, rutaSchema);

    }

}
