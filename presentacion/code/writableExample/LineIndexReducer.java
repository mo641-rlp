package org.rlazo;

import java.io.IOException;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

import org.rlazo.WordLineIndexWritable;

public class LineIndexReducer extends Reducer<Text, WordLineIndexWritable, Text, Text> {
    private static final String SEPARATOR = ",";

    @Override
    protected void reduce(Text key, Iterable<WordLineIndexWritable> values, Context context)
            throws IOException, InterruptedException {
        StringBuilder builder = new StringBuilder();
        for (WordLineIndexWritable value: values) {
            builder.append(value.toString());
            builder.append(SEPARATOR);
        }
        Text result = new Text(builder.substring(0, builder.length()-1));  // remove last SEPARATOR
        context.write(key, result);
    }
    
    

}
