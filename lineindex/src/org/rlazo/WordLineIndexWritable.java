package org.rlazo;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.WritableComparable;

public class WordLineIndexWritable implements WritableComparable<WordLineIndexWritable> {
    private Text filename = new Text();
    private long lineIndex;
    
    public WordLineIndexWritable() {}
    
    public WordLineIndexWritable(String word, long lineIndex) {
        set(word, lineIndex);        
    }
    
    public void set(String word, long lineIndex) {
        this.filename.set(word);
        this.lineIndex = lineIndex; 
    }

    @Override
    public void readFields(DataInput in) throws IOException {
        lineIndex = in.readLong();
        filename.readFields(in);
    }
    
    public static WordLineIndexWritable read(DataInput in) throws IOException {
        WordLineIndexWritable obj = new WordLineIndexWritable();
        obj.readFields(in);
        return obj;
    }

    @Override
    public void write(DataOutput out) throws IOException {
        out.writeLong(lineIndex);
        filename.write(out);
    }

    
    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof WordLineIndexWritable))
            return false;
        WordLineIndexWritable other = (WordLineIndexWritable)obj;
        return filename.equals(other.filename) && lineIndex == other.lineIndex;
    }

    @Override
    public int compareTo(WordLineIndexWritable o) {        
        WordLineIndexWritable other = (WordLineIndexWritable)o;        
        int result = filename.compareTo(other.filename);
        if (result == 0)
            if (lineIndex < other.lineIndex)
                result = -1;
            else
                result = lineIndex == other.lineIndex ? 0 : 1;
        return result;
    }
    
    @Override
    public int hashCode() {
        return filename.hashCode() + (int)(lineIndex % (Integer.MAX_VALUE-1));
    }

    @Override
    public String toString() {
        return filename.toString() + '@' + lineIndex;
    }
}
