package org.rlazo;

import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;

import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.lib.input.FileSplit;
import org.tartarus.snowball.ext.EnglishStemmer;

/**
 * @author rodrigo
 *
 */
public class LineIndexMapper extends Mapper<LongWritable, Text, Text, WordLineIndexWritable> {
    private static final String WORD_DELIMITERS_REGEXP = "[\\W_]+";
    private EnglishStemmer stemmer = new EnglishStemmer();
    
    /**
     * Stems an English word.
     *  
     * @param word String to process.
     * @return A string storing the root of word.
     */
    synchronized protected String stem(String word) {
        stemmer.setCurrent(word);
        stemmer.stem();
        return stemmer.getCurrent();
    }

    @Override
    protected void map(LongWritable offset, Text value, Context context)
            throws IOException, InterruptedException {
        Set<String> wordsAlreadySeen = new HashSet<String>();
        String filename = ((FileSplit)context.getInputSplit()).getPath().getName();
        WordLineIndexWritable result = new WordLineIndexWritable(filename, 
                offset.get());
        String line = value.toString();
        Text indexKey = new Text();
        for (String word: line.split(WORD_DELIMITERS_REGEXP)) {
            if (word.matches("^[0-9]+.*") || word.isEmpty()) continue;
            word = stem(word);
            if (!wordsAlreadySeen.contains(word)) {
                wordsAlreadySeen.add(word);
                indexKey.set(word.toLowerCase());
                context.write(indexKey, result);
            }           
        }        
    }

}
