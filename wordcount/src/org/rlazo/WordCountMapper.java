package org.rlazo;

import java.io.IOException;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;
import org.tartarus.snowball.ext.EnglishStemmer;;

class WordCountMapper extends Mapper<LongWritable,Text,Text,IntWritable> {
    private static final IntWritable ONE = new IntWritable(1);
    private static final String WORD_DELIMITERS_REGEXP = "[\\W_]+";
    private EnglishStemmer stemmer = new EnglishStemmer();
       
    /**
     * Stems an English word.
     *  
     * @param word String to process.
     * @return A string storing the root of word.
     */
    protected String stem(String word) {
        stemmer.setCurrent(word);
        stemmer.stem();
        return stemmer.getCurrent();
    }

    @Override
    protected void map(LongWritable key, Text value, Context context)
            throws IOException, InterruptedException {
        String line = value.toString();
        Text result = new Text();
        if (line.isEmpty()) return;
        for (String word: line.split(WORD_DELIMITERS_REGEXP)) {
            if (word.matches("^[0-9]+.*") || word.isEmpty()) 
                continue;
            stemmer.setCurrent(word);
            stemmer.stem();            
            result.set(stemmer.getCurrent().toLowerCase());
            context.write(result, ONE);
        }
    }
}
