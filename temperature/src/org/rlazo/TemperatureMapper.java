package org.rlazo;

import java.io.IOException;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

public class TemperatureMapper extends Mapper<LongWritable, Text, Text, IntWritable> {
    private static final int MISSING = 9999;
    private static final String QUALITY_REGEXP = "[01459]";
    
    @Override
    protected void map(LongWritable key, Text value,Context context)
            throws IOException, InterruptedException {
        String line = value.toString();
        String year = line.substring(15, 19);
        int airTemperature = Integer.parseInt(line.substring(88, 92));
        if (line.charAt(87) == '-') {
          airTemperature *= -1;
        }
        String quality = line.substring(92, 93);
        if (airTemperature != MISSING && quality.matches(QUALITY_REGEXP)) {
            context.write(new Text(year), new IntWritable(airTemperature));            
        } 
    }

}
